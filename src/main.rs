use reqwest::blocking::Client;
use reqwest::header;
use serde::Deserialize;
use std::env;
use std::path::Path;
use std::process::Command;

#[derive(Deserialize, Debug)]
struct Namespace {
    full_path: String,
}

#[derive(Deserialize, Debug)]
struct Repo {
    name: String,
    namespace: Namespace,
}

enum Action {
    Latexmk,
    MastBuild,
    Beam,
}

fn main() {
    // GitLab subgroup IDs
    const UNIT_ID: &str = "14059602";
    const DIAG_ID: &str = "14059608";
    const LECTURE_ID: &str = "51550387";
    let pat: String = match std::fs::read_to_string(Path::new("PAT")) {
        Ok(pat) => {
            pat
        }
        Err(err) => {
            panic!("{}", err);
        }
    };
    let cwd = env::current_dir().unwrap();
    let mut headers = header::HeaderMap::new();
    headers.insert(
        "PRIVATE-TOKEN",
        header::HeaderValue::from_str(pat.trim()).unwrap_or_else(|_| panic!(
            "The contents of `PAT`:\n\t{}\ncould not be converted to a valid HeaderValue",
            &pat.trim()
        )),
    );
    let client = Client::builder().default_headers(headers).build().unwrap();

    let repos = |id: &str| -> Vec<Repo> {
        client
            .get(&format!("https://gitlab.com/api/v4/groups/{}/projects", id))
            .send()
            .unwrap()
            .json::<Vec<Repo>>()
            .unwrap()
    };
    let build = |id: &str, target_path: &Path, action: Action| {
        if !target_path.exists() {
            std::fs::create_dir(target_path)
                .unwrap_or_else(|_| panic!("Could not create directory {:?}", target_path));
        }
        env::set_current_dir(target_path).unwrap_or_else(|_| panic!(
            "Failed to set current directory to {:?}",
            target_path
        ));
        for repo in repos(id) {
            let name = &repo.name;
            let url = &(String::from("https://gitlab.com/")
                + &repo.namespace.full_path
                + "/"
                + name
                + ".git");
            let repo_path = &Path::new(name);
            if repo_path.is_dir() {
                env::set_current_dir(repo_path).unwrap_or_else(|_| panic!(
                    "Couldn't set current working directory to {:?}",
                    repo_path
                ));
                let git = Command::new("git").arg("pull").output().unwrap_or_else(|_| panic!(
                    "Could not execute `git pull` on {:?}, make sure you have git installed",
                    repo_path
                ));
                if git.status.success() {
                    println!("`git pull` on {:?} succeeded", repo_path);
                } else {
                    eprintln!("`git pull` on {:?} failed", repo_path);
                }
            } else {
                let git = Command::new("git").arg("clone").arg(url).output().unwrap_or_else(|_| panic!(
                    "Could not execute `git clone` for repo `{}` in directory {:?}, make sure you have git installed",
                    name,
                    target_path
                ));
                println!("{}", url);
                if git.status.success() {
                    println!(
                        "`git clone` on repo `{}` in directory {:?} succeeded",
                        name, target_path
                    );
                } else {
                    eprintln!(
                        "`git clone` on repo `{}` in directory {:?} failed",
                        name, target_path
                    );
                }
                println!("{:?}, {}", env::current_dir().unwrap(), name);
                env::set_current_dir(name).unwrap_or_else(|_| panic!(
                    "Couldn't set current working directory to {:?}",
                    repo_path
                ));
            }
            let beam_path = Path::new("slides.beam");
            let command = match &action {
                Action::MastBuild => Command::new("mast-build").output(),
                Action::Latexmk => Command::new("latexmk").arg("-pdf").output(),
                Action::Beam => if beam_path.exists() {
                    Command::new("beam").arg("slides.beam").output()
                } else {
                    Command::new("latexmk").arg("-pdf").output()
                },
            };
            let cmd_name: &str = match &action {
                Action::Latexmk => "latexmk",
                Action::MastBuild => "mast-build",
                Action::Beam => if beam_path.exists() {
                    "beam"
                } else {
                    "latexmk"
                },
            };
            let output = command.expect("Failed to execute `latexmk`, make sure it is installed");
            if output.status.success() {
                println!(
                    "`{}` on repo `{}` in directory {:?} succeeded",
                    cmd_name, name, repo_path
                );
            } else {
                eprintln!(
                    "`{}` on repo `{}` in directory {:?} failed",
                    cmd_name, name, repo_path
                );
            }
            env::set_current_dir("../").unwrap();
        }
        env::set_current_dir(&cwd).unwrap();
    };

    build(UNIT_ID, Path::new("units"), Action::MastBuild);
    build(DIAG_ID, Path::new("diagnostics"), Action::Latexmk);
    build(LECTURE_ID, Path::new("lectures"), Action::Beam);
}
